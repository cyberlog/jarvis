
# JARVIS: A Sassy Virtual Assistant AI

Jarvis is a modified version of [Mycroft AI](https://mycroft.ai/) to become more sassy, as well as adding additional functionality. 

## Installation

1. Setup and install Mycroft on your linux box.
2. In the settings on the website, change the wake word from "Hey Mycroft" to "Hey Jarvis".
3. Clone this repository and launch `setup.sh` from inside the directory.

## Usage
After setting up the files, its time to have fun.
Navigate to the mycroft-core folder and launch it with the command ` ./start-mycroft.sh all`.
Its at this point that I like to add it to my list of programs that launch on startup. However, Mycroft is slow and takes up RAM. Be warned. 


### Skills

Prefix all skills with "Hey JARVIS" and wait for the ding before continuing.
- Punch it: Plays Sotify (premium not needed)
- Next Song: Skips a song in Spotify
- Pause Music: Pauses song in Spotify
- Who are you?
- Shut up
- Miss-pronounce word: Sassy reply
- Watch the Sass

## Credits

Obviously JARVIS is a slight modification to the Mycroft AI project. Those people deserve the real credit. 

## Contact
If you have any questions comments or tips for improvement, message me on discord at Jay#3130. Have fun and stay Jaynky.
