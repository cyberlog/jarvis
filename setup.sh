#!/usr/bin/env bash

cp -r -f local-skills/* /opt/mycroft/skills
cp -r local-skills/fallback/dialog /opt/mycroft/skills/fallback-unknown.mycroftai/