# Copyright 2016 Mycroft AI, Inc.
#
# This file is part of Mycroft Core.
#
# Mycroft Core is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Mycroft Core is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Mycroft Core.  If not, see <http://www.gnu.org/licenses/>.

import time 
import os
import psutil
import subprocess
from adapt.intent import IntentBuilder

from mycroft.skills.core import MycroftSkill
from mycroft.util.log import getLogger

__author__ = 'eward'



LOGGER = getLogger(__name__)


class HelloWorldSkill(MycroftSkill):
    def __init__(self):
        super(HelloWorldSkill, self).__init__(name="HelloWorldSkill")

    def initialize(self):
        thank_you_intent = IntentBuilder("ThankYouIntent"). \
            require("ThankYouKeyword").build()
        self.register_intent(thank_you_intent, self.handle_thank_you_intent)


##Linux Commands

#list directoryGlitch633



#how are you
        how_are_you_intent = IntentBuilder("HowAreYouIntent"). \
            require("HowAreYouKeyword").build()
        self.register_intent(how_are_you_intent,
                             self.handle_how_are_you_intent)

        shut_up_intent = IntentBuilder("ShutUpIntent"). \
            require("ShutUpKeyword").build()
        self.register_intent(shut_up_intent,
                             self.handle_shut_up_intent)

#Hello World
        hello_world_intent = IntentBuilder("HelloWorldIntent"). \
            require("HelloWorldKeyword").build()
        self.register_intent(hello_world_intent,
                             self.handle_hello_world_intent)

#Do you want to hear a joke
        hear_joke_intent = IntentBuilder("HearJokeIntent"). \
            require("HearJokeKeyword").build()
        self.register_intent(hear_joke_intent,
                             self.handle_hear_joke_intent)

#plays a joke que
        punch_line_intent = IntentBuilder("PunchLineIntent"). \
            require("PunchLineKeyword").build()
        self.register_intent(punch_line_intent,
                             self.handle_punch_line_intent)

#pause music
        #pause_music_intent = IntentBuilder("PauseMusicIntent"). \
        #    require("PauseMusicKeyword").build()
        #self.register_intent(pause_music_intent,
        #                     self.handle_pause_music_intent)

#Fight Me 
        fight_me_intent = IntentBuilder("FightMeIntent"). \
            require("FightMeKeyword").build()
        self.register_intent(fight_me_intent,
                             self.handle_fight_me_intent)

#play music
        punch_it_intent = IntentBuilder("PunchItIntent"). \
            require("PunchItKeyword").build()
        self.register_intent(punch_it_intent,
                             self.handle_punch_it_intent)
  #  if time.strftime('%H') == 9:   


    def handle_thank_you_intent(self, message):
        self.speak_dialog("welcome")
        os.system("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next")
        
    

    def handle_how_are_you_intent(self, message):
        #cpu = str(int(psutil.cpu_percent()))
        #date = mycroft.util.extract_datetime(text, anchorDate=None, lang='en-us')
        #self.speak_dialog(cpu)
        #self.speak_dialog()
        self.speak_dialog("how.are.you")
        

    #shut up
    def handle_shut_up_intent(self, message):
        subprocess.run(["play","~/Music/soundeffects/jab.mp3"])
        self.speak_dialog("no.you")

    #play music
    def handle_punch_it_intent(self, message):
        os.system("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play")
        self.speak_dialog("hit.it")
    
    def handle_hear_joke_intent(self, message):
        subprocess.run(["play","~/Music/soundeffects/torture.mp3"])
 

    def handle_fight_me_intent(self, message):
        self.speak_dialog("take.this")
        subprocess.run(["play","~/Music/soundeffects/lefthook.mp3"])
        subprocess.run(["play","~/Music/soundeffects/righthook.mp3"])
        subprocess.run(["play","~/Music/soundeffects/uppercut.mp3"])
        subprocess.run(["play","~/Music/soundeffects/rightcross.mp3"])
        subprocess.run(["play","~/Music/soundeffects/jab.mp3"])
        subprocess.run(["play","~/Music/soundeffects/uppercut.mp3"])
        subprocess.run(["play","~/Music/soundeffects/righthook.mp3"])
 
    def handle_pause_intent(self, message):
        self.speak_dialog("no.you")


#pause music
    def handle_punch_line_intent(self, message):
        os.system("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Pause")
        self.speak_dialog("pause.music")
    

#watch the sass
    def handle_hello_world_intent(self, message):
        self.speak_dialog("hello.world")

    def stop(self):
        pass


def create_skill():
    return HelloWorldSkill()
